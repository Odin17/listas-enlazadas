package ListasDobles;
import cola.ColaDinamica;
public class Principal {
  

public static void main(String[] args) {
   
    long inicio = System.nanoTime();
   
   ListaDoble miLista = new ListaDoble();
   rellenarCola(miLista);
   miLista.mostrarListaFinInicio();
   miLista.eliminarDelInicio();
   miLista.mostrarListaFinInicio();
   System.out.println("Se fue:"+miLista.eliminarDelFinal());
    long fin = System.nanoTime();
    double dif =(double)(fin-inicio)*30000.0e-9;
    System.out.println("tiempo de atención:"+ dif+ "-segundos de toda la cola.");
    
} 
public static void rellenarCola(ListaDoble NodoDoble){
        
        //Genero el numero de personas entre 1 y 50
        int numPersonas = Metodos.generaNumeroAleatorio(10, 50);
        
        System.out.println("Se van a generar: "+numPersonas+" personas");
        
        Tiempo p;
        int segundos;
        for (int i = 10; i < numPersonas; i++) {
            
            //Genero la edad de la persona
            segundos = Metodos.generaNumeroAleatorio(10, 15);
            
            //Creo la persona
            p = new Tiempo(segundos);
            
            System.out.println("A incresado una persona en:"+p.getsegundos()+" segundos");
            
            NodoDoble.agregarAlInicio(i);
            
            
        }
        
}
}
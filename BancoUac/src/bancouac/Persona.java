package bancouac;

public class Persona {
    
    private int segundos;
    
    /**
     * Constructor por defecto
     * @param segundos
     */
    public Persona(int segundos){
        this.segundos=segundos;
    }
    /**
     * Devuelve la edad
     * @return Edad acutal
     */
    public int getsegundos() {
        return segundos;
    }

    public void setsegundos(int segundos) {
        this.segundos = segundos;
    }
    
}